
resource "aws_instance" "vms" {
  ami               = var.ami
  instance_type     = var.instype
  tags = {
    "Name" = "demo-vm"
  }
}

resource "aws_instance" "vms1" {
  ami               = var.ami
  instance_type     = var.instype
  tags = {
    "Name" = "demo-vm1"
  }
}