terraform {
  backend "s3" {
    bucket = "terraformbackend0525"
    key    = "terraform.tfstate"
    dynamodb_table = "ashis-terraformstatelock"
    region     = "us-east-1"
  }
}

provider "aws" {
  # Configuration options
  region     = "us-east-1"
}